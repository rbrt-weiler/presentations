# Network Access Control

Safeguarding your network access

---

# Agenda

@ol

- What is NAC?
- Why do I need NAC?
- What now?

@olend

---

# What is NAC?

Some theoretical and practical notes.

+++

## NAC is a piece of software.

It's just a piece of software that gathers information, makes an informed decision and tells the switch what to do.

+++

## NAC is the first line of defense.

With the decision made by NAC, a switch will just allow traffic that is explicitly permitted for a given connected device. This may be all traffic, no traffic at all, or something in between.